module.exports = (csv) => {
  // eslint-disable-next-line no-unused-vars
  const [columns, ...rows] = csv.split('\n');
  return rows.map(row => row.split(',').join(' '));
};
