const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const Path = require('path');

const Trie = require('./Trie');
const csvToArr = require('./csvToArr');

const csv = fs.readFileSync(Path.join(__dirname, 'data.csv'), { encoding: 'utf8' });
const names = csvToArr(csv);
const trieNames = new Trie();
names.map(name => trieNames.add(name));

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('index');
});

app.post('/names', (req, res) => {
  const { name } = req.body;
  if (name.length < 3) {
    return res.status(500).send('Error: min 3 length');
  }
  return res.json(trieNames.getAll(name));
});

app.listen(process.env.PORT || 3000, () => console.log('Server Started')); // eslint-disable-line
