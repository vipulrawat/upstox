/* eslint-disable func-names */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
function Trie() {
  this.head = {
    key: '',
    children: {},
  };
}
const childTree = (string, tree) => {
  let node = tree;
  while (string) {
    node = node.children[string[0]];
    // eslint-disable-next-line no-param-reassign
    string = string.slice(1);
  }
  return node;
};
Trie.prototype.add = function (data) {
  let key = data;
  let curNode = this.head;
  let newNode = null;
  let curChar = key.slice(0, 1);
  key = key.slice(1);

  while (typeof curNode.children[curChar] !== 'undefined'
    && curChar.length > 0) {
    curNode = curNode.children[curChar];
    curChar = key.slice(0, 1);
    key = key.slice(1);
  }

  while (curChar.length > 0) {
    newNode = {
      key: curChar,
      value: key.length === 0 ? null : undefined,
      children: {},
    };

    curNode.children[curChar] = newNode;

    curNode = newNode;

    curChar = key.slice(0, 1);
    key = key.slice(1);
  }
};

Trie.prototype.search = function (data) {
  let key = data;
  let curNode = this.head;
  let curChar = key.slice(0, 1);
  let d = 0;

  key = key.slice(1);

  while (typeof curNode.children[curChar] !== 'undefined' && curChar.length > 0) {
    curNode = curNode.children[curChar];
    curChar = key.slice(0, 1);
    key = key.slice(1);
    d += 1;
  }

  if (curNode.value === null && key.length === 0) {
    return d;
  }
  return -1;
};

Trie.prototype.getAll = function (string) {
  const allWords = [];
  const iterate = (startString, tree) => {
    for (const k in tree.children) {
      const child = tree.children[k];
      const newString = startString + child.key;
      if (child.value === null) {
        allWords.push(newString);
      }
      iterate(newString, child);
    }
  };
  const remaining = childTree(string, this.head);
  if (remaining) {
    iterate(string, remaining);
  }
  return allWords;
};

module.exports = Trie;
